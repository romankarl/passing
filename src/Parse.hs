module Parse (patterns) where

import Data.Foldable (toList)
import Data.Maybe (fromMaybe)
import Control.Monad (join)
import Data.List (findIndex, transpose, unfoldr)
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NonEmpty
import Text.Parsec

import ListWithPairs (ListWithPairs(Nil, Single, Pair))
import qualified ListWithPairs
import Pattern

patterns :: FilePath -> IO (Either ParseError [Either BasicPattern AsyncPattern])
patterns filename = parse (oneOfPatterns `sepEndBy` endOfLine) filename <$> readFile filename

oneOfPatterns :: Parsec String () (Either BasicPattern AsyncPattern)
oneOfPatterns = Left <$> jpattern <|> Right <$> asyncPattern

mkPattern ::
  Tact ->
  ListWithPairs Tact ->
  Maybe [Hand] ->
  [(Maybe [Juggler], NonEmpty BasicThrow)] ->
  BasicPattern
mkPattern tactsPerTime os hands patternLines = BasicPattern tactsPerTime (map snd patternLines) $
  mkPatternReferences (map fst patternLines) os (fromMaybe (toList defaultHands) hands)
    where
      defaultHands = ListWithPairs.map' defaultHand (const $ const (RightHand, LeftHand)) os
      defaultHand o
        | even $ o `mod` tactsPerTime = RightHand
        | otherwise = LeftHand

mkPatternReferences ::
  [Maybe [Juggler]] -> ListWithPairs Tact -> [Hand] -> ListWithPairs PatternReference
mkPatternReferences mjs os = ListWithPairs.zipWith ($) $
  ListWithPairs.zipWith (flip PatternReference) os (indices $ sequence mjs)
    where
      indices Nothing | length mjs == 1 = repeat 0
      indices Nothing = [0..]
      indices (Just js) = unfoldr (\j -> flip (,) (j+1) <$> findIndex (elem j) js) 0

jpattern :: Parsec String () BasicPattern
jpattern = mkPattern <$> fmap (read . pure) digit <* char '-' <*> offsets <* endOfLine <*>
  optionMaybe (many1 startHand <* endOfLine) <*>
  patternLine `endBy1` endOfLine

asyncPattern :: Parsec String () AsyncPattern
asyncPattern = AsyncPattern <$ string "siteswap" <*> numberOrStartHands <* endOfLine <*> asyncThrows

numberOrStartHands :: Parsec String () [AsyncStartHand]
numberOrStartHands = char '-' *> (asyncDefaultStartHands <$> juggler') <|>
  string " (" *> asyncStartHands <* char ')'

asyncDefaultStartHands :: Juggler -> [AsyncStartHand]
asyncDefaultStartHands = zipWith AsyncStartHand [0..] . asyncDefaultStartRawHands

asyncDefaultStartRawHands :: Juggler -> [Hand]
asyncDefaultStartRawHands 2 = [RightHand, RightHand]
asyncDefaultStartRawHands n = take n $ cycle [RightHand, LeftHand]

asyncStartHands :: Parsec String () [AsyncStartHand]
asyncStartHands = uncurry mkAsyncStartHand . unzip <$> asyncStartHands'
  where
    asyncStartHands' = ((,) <$> juggler <*> optionMaybe startHand) `sepBy1` char ' '
    mkAsyncStartHand js hands = zipWith AsyncStartHand js .
      fromMaybe (asyncDefaultStartRawHands $ length js) $
      sequenceA hands

asyncThrows :: Parsec String () (NonEmpty Tact)
asyncThrows = NonEmpty.fromList . join . transpose <$> number `sepBy1` char ' ' `endBy1` endOfLine

patternLine :: Parsec String () (Maybe [Juggler], NonEmpty BasicThrow)
patternLine = try ((,) <$> (Just <$> jugglers <* string ": ") <*> soloPattern) <|>
  (,) Nothing <$> soloPattern

startHand :: Parsec String () Hand
startHand = RightHand <$ char 'r' <|> LeftHand <$ char 'l'

jugglers :: Parsec String () [Juggler]
jugglers = juggler `sepBy1` char ','

juggler :: Parsec String () Juggler
juggler = subtract 1 <$> juggler'

juggler' :: Parsec String () Juggler
juggler' = read . pure <$> digit

offsets :: Parsec String () (ListWithPairs Tact)
offsets = Single <$> number <*> os <|>
  Pair <$ char '(' <*> number <* char ' ' <*> number <* char ')' <*> os
    where os = option Nil (char ' ' *> offsets)

number :: Parsec String () Int
number = read <$> many1 digit

soloPattern :: Parsec String () (NonEmpty BasicThrow)
soloPattern = NonEmpty.fromList <$> throwWithHurry `sepBy1` char ' '

throwWithHurry :: Parsec String () BasicThrow
throwWithHurry = flip ($) <$> optionMaybe (Hurry <$ char 'h' <|> Pause <$ char 'p') <*> throw

throw :: Parsec String () (Maybe HandIrregularity -> BasicThrow)
throw = BasicThrow <$> (char '[' *> (target `sepBy1` char ' ') <* char ']' <|> (:[]) <$> target)

target :: Parsec String () BasicTarget
target = BasicTarget <$> fmap read (many1 digit) <*> option const pass

pass :: Parsec String () (Juggler -> Juggler -> Juggler)
pass = (\j1 js -> (j1 + 1) `mod` js) <$ char 'p' <|>
  char '|' *> targetJuggler

targetJuggler :: Parsec String () (Juggler -> Juggler -> Juggler)
targetJuggler = (\j j1 js -> (j1 + j) `mod` js) <$> (char '+' *> juggler') <|>
  (\j j1 js -> (j1 - j) `mod` js) <$> (char '-' *> juggler') <|>
  (\j _ _ -> j) <$> juggler
