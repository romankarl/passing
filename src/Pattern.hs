module Pattern (
  Tact, Time, Juggler, Point(Point), BasicThrow(BasicThrow), Hand(..), HandIrregularity(..),
  PatternReference(PatternReference), BasicPattern(BasicPattern), BasicTarget(BasicTarget),
  AsyncPattern(AsyncPattern), AsyncStartHand(AsyncStartHand), FinitePattern, Connection(Connection),
  allCoordinates, allPoints, lastJuggler, throwSequences, transform) where

import Control.Monad (join)
import Data.Foldable (toList)
import Data.Function (on)
import Data.List (sortOn, transpose, unfoldr) 
import Data.List.NonEmpty (NonEmpty((:|)))
import qualified Data.List.NonEmpty as NonEmpty
import Data.Map (fromListWith)
import qualified Data.Map as Map
import Diagrams.Names (IsName)

import ListWithPairs (ListWithPairs)
import qualified ListWithPairs

type Tact = Int
type Time = Int
type Juggler = Int
data Point = Point Juggler Tact deriving (Show, Eq, Ord)

instance IsName Point

data Hand = LeftHand | RightHand
data HandIrregularity = Hurry | Pause
data BasicTarget = BasicTarget Tact (Juggler -> Juggler -> Juggler)
data BasicThrow = BasicThrow [BasicTarget] (Maybe HandIrregularity)
data PatternReference = PatternReference Int Tact Hand
data BasicPattern = BasicPattern Tact [NonEmpty BasicThrow] (ListWithPairs PatternReference)

data AsyncStartHand = AsyncStartHand {
  aJuggler :: Juggler,
  _aStartHand :: Hand}
data AsyncPattern = AsyncPattern [AsyncStartHand] (NonEmpty Tact)

data CyclicTarget = CyclicTarget {
  cTactDiff :: Tact,
  _cRecipient :: Juggler }
data CyclicThrow =  CyclicThrow {
  cTargets :: [CyclicTarget],
  cIrregularity :: Maybe HandIrregularity }
data CyclicSoloPattern = CyclicSoloPattern {
  cThrows :: NonEmpty CyclicThrow,
  _cOffset :: Tact,
  _cStartHand :: Hand }
data CyclicPattern = CyclicPattern Tact (ListWithPairs CyclicSoloPattern)
data FixedThrow = FixedThrow {
  _fHand :: Maybe Hand,
  fStartTact :: Tact,
  fRecipient :: [Point] }
data ContinuousPattern = ContinuousPattern Tact (ListWithPairs [FixedThrow])
data FinitePattern = FinitePattern {
  _fTactsPerTime :: Tact,
  fSoloPatterns :: ListWithPairs [FixedThrow] }
data Connection = Connection Point Point Bool

transform :: Time -> Either BasicPattern AsyncPattern -> FinitePattern
transform time = finitePattern time . continuousPattern . relativePattern . cyclicPattern

throwSequences :: FinitePattern -> [Connection]
throwSequences = concat . ListWithPairs.zipWith' ((=<<) . withThrower) [0..] . fSoloPatterns
  where
    withThrower j1 (FixedThrow _ t1 targets) =
      (\ (k,v) -> Connection (Point j1 t1) k (v>1)) <$> accum targets
    accum targets = Map.toList $ fromListWith (+) (flip (,) (1 :: Int) <$> targets)

lastJuggler :: FinitePattern -> Juggler
lastJuggler (FinitePattern _ soloPatterns) = length soloPatterns - 1

allPoints :: FinitePattern -> [(Point, Maybe Hand)]
allPoints = concat . ListWithPairs.zipWith' (fmap . jugglerPoint) [0..] . fSoloPatterns
  where
    jugglerPoint j1 (FixedThrow h t1 _) = (Point j1 t1, h)

allCoordinates :: FinitePattern -> [(Double, Double)]
allCoordinates jpattern@(FinitePattern tactsPerTime soloPatterns) =
  coordinatesFor (jugglerCoordinates soloPatterns) tactsPerTime <$> map fst (allPoints jpattern)

jugglerCoordinates :: ListWithPairs a -> [Double]
jugglerCoordinates ps = (/3) . fromIntegral . negate <$> toList jugglerCoordinates'
  where
    jugglerCoordinates' :: ListWithPairs Int
    jugglerCoordinates' = ListWithPairs.scan' (\a _ -> a+3) (\a _ _ -> (a+1, a+4)) 0 ps

coordinatesFor :: [Double] -> Tact -> Point -> (Double, Double)
coordinatesFor js tactsPerTime (Point j t) = (on (/) fromIntegral t tactsPerTime, js!!j)

cyclicPattern :: Either BasicPattern AsyncPattern -> CyclicPattern
cyclicPattern = either basicToCyclicPattern asyncToCyclicPattern

basicToCyclicPattern :: BasicPattern -> CyclicPattern
basicToCyclicPattern (BasicPattern tactsPerTime soloPatterns refs) =
  CyclicPattern tactsPerTime $ ListWithPairs.zipWith cyclicSoloPattern refs [0..]
    where
      cyclicSoloPattern (PatternReference i offset h) j1 =
        CyclicSoloPattern (fixTargets j1 <$> soloPatterns!!i) offset h
      fixTargets j1 (BasicThrow targets i) = CyclicThrow (fixRecipient j1 <$> targets) i
      fixRecipient j1 (BasicTarget t recipient) = CyclicTarget t (recipient j1 (length refs))

asyncToCyclicPattern :: AsyncPattern -> CyclicPattern
asyncToCyclicPattern (AsyncPattern startHands globalPattern) =
  CyclicPattern numJugglers . ListWithPairs.fromList . fmap snd . sortOn fst $
  zipWith3 cyclicSoloPattern [0..] startHands localPatterns
    where
      numJugglers = length startHands
      cyclicSoloPattern offset (AsyncStartHand juggler hand) localPattern =
        (juggler, CyclicSoloPattern (fixRecipient offset <$> localPattern) offset hand)
      fixRecipient tactJ1 tact =
        CyclicThrow
          [CyclicTarget tact . aJuggler $ startHands !! ((tactJ1 + tact) `mod` numJugglers)]
          Nothing
      localPatterns =
        fmap NonEmpty.fromList .
        transpose .
        unfoldr (\as -> if null as then Nothing else Just $ splitAt numJugglers as) .
        join $
        replicate numJugglers (toList globalPattern)

relativePattern :: CyclicPattern -> CyclicPattern
relativePattern (CyclicPattern tactsPerTime soloPatterns) =
  CyclicPattern tactsPerTime $ fmap modifySoloPattern soloPatterns
    where
      modifySoloPattern p = p { cThrows = modifyThrow <$> cThrows p }
      modifyThrow t = t { cTargets = modifyTarget <$> cTargets t}
      modifyTarget t = t { cTactDiff = cTactDiff t - base * tactsPerTime }
      base = if isMultiplex soloPatterns then 2 else
        baseThrow (ListWithPairs.length' soloPatterns) numProps (ListWithPairs.isFlat soloPatterns)
      numProps = props (fmap (fmap (fmap cTactDiff . cTargets) . cThrows) soloPatterns) tactsPerTime

continuousPattern :: CyclicPattern -> ContinuousPattern
continuousPattern (CyclicPattern tactsPerTime soloPatterns) = ContinuousPattern tactsPerTime $
  ListWithPairs.map'
    (continuousSoloPattern False tactsPerTime)
    ((\f a1 a2 -> (f a1, f a2)) $ continuousSoloPattern True tactsPerTime)
    soloPatterns

continuousSoloPattern :: Bool -> Tact -> CyclicSoloPattern -> [FixedThrow]
continuousSoloPattern singleHand tactsPerTime (CyclicSoloPattern throws offset startHand) =
  zipWith3 (\ (CyclicThrow targets _) t1 h -> FixedThrow h t1 $ toFRecipient t1 <$> targets)
    continuousThrows
    ts
    (handSequence (cIrregularity <$> continuousThrows) singleHand startHand)
      where
        tptAdapted = if singleHand then 2 * tactsPerTime else tactsPerTime
        shift = length throws - offset `div` tptAdapted
        firstTact = offset `mod` tptAdapted
        ts = [firstTact, firstTact + tptAdapted ..]
        continuousThrows = NonEmpty.drop shift $ NonEmpty.cycle throws
        toFRecipient t1 (CyclicTarget td j2) = Point j2 (t1+td)

finitePattern :: Time -> ContinuousPattern -> FinitePattern
finitePattern time (ContinuousPattern tactsPerTime soloPatterns) = FinitePattern tactsPerTime $
  ListWithPairs.zipWith (finiteSoloPattern time tactsPerTime) soloPatterns [0..]

finiteSoloPattern :: Time -> Tact -> [FixedThrow] -> Juggler -> [FixedThrow]
finiteSoloPattern time tactsPerTime throws j1 =
  maybeTarget <$> takeWhile ((<= numTacts) . fStartTact) throws
    where
      maybeTarget t@(FixedThrow _ t1 ps) = t { fRecipient = filter (withinBounds t1) ps }
      numTacts = time * tactsPerTime
      withinBounds t1 p2@(Point _ t2) = Point j1 t1 /= p2 && 0 <= t2 && t2 <= numTacts

props :: ListWithPairs (NonEmpty [Tact]) -> Tact -> Int
props patterns tactsPerTime =
  (sum . join) (rps nps >>= NonEmpty.toList) `div` (tactsPerTime * period nps)
    where
      nps = ListWithPairs.mergePairs (\a b -> join $ rps (a :| [b])) patterns
      rps ps = fmap (NonEmpty.fromList . NonEmpty.take (period ps) . NonEmpty.cycle) ps
      period ps = foldr (lcm . NonEmpty.length) 1 ps

isMultiplex :: ListWithPairs CyclicSoloPattern -> Bool
isMultiplex = any $ any ((> 1) . length . cTargets) . cThrows

baseThrow :: Juggler -> Int -> Bool -> Int
baseThrow numJugglers numProps True = numProps `div` numJugglers
baseThrow numJugglers numProps False = numProps `div` (2*numJugglers) * 2

handSequence :: [Maybe HandIrregularity] -> Bool -> Hand -> [Maybe Hand]
handSequence _ True h = repeat $ Just h
handSequence [] _ _ = []
handSequence (Nothing : is) False h = Just h : handSequence is False (otherHand h)
handSequence (Just Hurry : is) False h = Just (otherHand h) : handSequence is False h
handSequence (Just Pause : is) False h = Nothing : handSequence is False h

otherHand :: Hand -> Hand
otherHand LeftHand = RightHand
otherHand RightHand = LeftHand
