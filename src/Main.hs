{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

import Data.Function (on)
import Data.List (intersect)
import Diagrams.Prelude
import Diagrams.Backend.SVG (B, renderSVG)
import System.Environment (getArgs)

import Parse (patterns)
import Pattern

data Arc = HorizontalClockwise | HorizontalAntiClockwise | Diagonal

time :: Pattern.Time
time = 7

inputPath :: String -> FilePath
inputPath name = concat ["diagrams/", name, ".txt"]

svgPath :: String -> FilePath
svgPath name = concat ["diagrams/", name, ".svg"]

sizeSpec :: SizeSpec V2 Double
sizeSpec = mkSizeSpec $ V2 (Just 1000) Nothing

main :: IO ()
main = do
  name <- head <$> getArgs
  patterns (inputPath name) >>=
    either (\a -> print a >> pure []) pure >>=
    renderSVG (svgPath name) sizeSpec . example

example :: [Either BasicPattern AsyncPattern] -> Diagram B
example p = lwL 0.05 . hsep 1 . map (vsep 1) . columns $
  map (causalDiagram . Pattern.transform time) p
    where
      columns | length p > 5 = (\(a,b) -> [a,b]) . splitAt ((length p + 1) `div` 2)
        | otherwise = (:[])

causalDiagram :: FinitePattern -> Diagram B
causalDiagram p = points p #
  conn (map fst $ allPoints p) (lastJuggler p) (throwSequences p)

points :: FinitePattern -> Diagram B
points p = atPoints (map p2 $ allCoordinates p) $ map (uncurry $ flip node) (allPoints p)

node :: IsName a => Maybe Hand -> a -> Diagram B
node Nothing = (circle 0.05 # lw (local 0.025) #) . named
node (Just LeftHand) = (circle 0.05 # fc white #) . named
node (Just RightHand) = (circle 0.05 # fc black #) . named

conn :: [Pattern.Point] -> Juggler -> [Connection] -> Diagram B -> Diagram B
conn ps jMax = flip foldr id $
  \ (Connection po1@(Pattern.Point j1 t1) po2@(Pattern.Point j2 t2) multi) ->
  let
    jugglerDiff = j2 - j1
    tactDiff = t2 - t1
    (jInc, tInc) = on (,) (`div` gcd jugglerDiff tactDiff) jugglerDiff tactDiff
    intersections = zipWith Pattern.Point
      [j1 + jInc, j1 + 2*jInc .. j2 - jInc] [t1 + tInc, t1 + 2*tInc .. t2 - tInc]
    a | null (intersections `intersect` ps) = Nothing
      | otherwise = Just $
        if jugglerDiff == 0 then
          if (j1 == jMax) == (t1 < t2) then HorizontalAntiClockwise
          else HorizontalClockwise
        else Diagonal
  in (.) $ connect' (arrowArgs a (if multi then Just () else Nothing)) po1 po2

arrowArgs :: Maybe Arc ->  Maybe () -> ArrowOpts Double
arrowArgs = foldr (\ _ -> shaftStyle %~ lwL 0.07) .
  foldr (\a -> arrowShaft .~ arc xDir (arcAngle a @@ turn))
    (with & gaps .~ local 0.1 & headLength .~ local 0.2)
    where
      arcAngle HorizontalClockwise = -1/5
      arcAngle HorizontalAntiClockwise = 1/5
      arcAngle Diagonal = -1/8
