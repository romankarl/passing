{-# LANGUAGE DeriveFunctor, DeriveFoldable #-}

module ListWithPairs (
  ListWithPairs(..), fromList, isFlat, length', map', mergePairs, scan', zipWith, zipWith') where

import Prelude hiding (zipWith)

data ListWithPairs a =
  Nil |
  Single a (ListWithPairs a) |
  Pair a a (ListWithPairs a)
  deriving (Functor, Foldable, Show)

zipWith :: (a -> b -> c) -> ListWithPairs a -> [b] -> ListWithPairs c
zipWith _ Nil _ = Nil
zipWith _ _ [] = Nil
zipWith f (Single x xs) (z:zs) = Single (f x z) $ zipWith f xs zs
zipWith _ Pair{} [_] = Nil
zipWith f (Pair x y xs) (z1:z2:zs) = Pair (f x z1) (f y z2) $ zipWith f xs zs

zipWith' :: (a -> b -> c) -> [a] -> ListWithPairs b -> ListWithPairs c
zipWith' = flip . ListWithPairs.zipWith . flip

map' :: (a -> b) -> (a -> a -> (b,b)) -> ListWithPairs a -> ListWithPairs b
map' _ _ Nil = Nil
map' f g (Single x xs) = Single (f x) $ map' f g xs
map' f g (Pair x y xs) = uncurry Pair (g x y) $ map' f g xs

scan' :: (b -> a -> b) -> (b -> a -> a -> (b,b)) -> b -> ListWithPairs a -> ListWithPairs b
scan' _ _ _ Nil = Nil
scan' f g z (Single x xs) = Single z $ scan' f g (f z x) xs
scan' f g z (Pair x y xs) = let (z1,z2) = g z x y in Pair z z1 $ scan' f g z2 xs

mergePairs :: (a -> a -> a) -> ListWithPairs a -> [a]
mergePairs _ Nil = []
mergePairs f (Single x xs) = x : mergePairs f xs
mergePairs f (Pair x y xs) = f x y : mergePairs f xs

length' :: ListWithPairs a -> Int
length' Nil = 0
length' (Single _ xs) = 1 + length' xs
length' (Pair _ _ xs) = 1 + length' xs

isFlat :: ListWithPairs a -> Bool
isFlat a = length a == length' a

fromList :: [a] -> ListWithPairs a
fromList = foldr Single Nil
